#!/bin/bash

LIBS_DIR="libs"
CONFIG_DIR="config"

source $CONFIG_DIR/program.ini
source $LIBS_DIR/matrix.lib
source $LIBS_DIR/system.lib
source $LIBS_DIR/functions.lib
source $LIBS_DIR/drawing/drawing.lib
source $LIBS_DIR/entity/entity.lib
source $LIBS_DIR/entity/entity_go.lib
source $LIBS_DIR/entity/ai.lib
source $LIBS_DIR/entity/memory.lib
source $LIBS_DIR/entity/feelings.lib
source $LIBS_DIR/drawing/drawing_step.lib
source $LIBS_DIR/drawing/drawing_memory.lib

#[SCENARIO]
filling
filling_type "$ENUM_MATRIX_FOOD" "$FOOD_PROBABILITY"

while true; do
  action_entity
  output="$(clear; drawing_matrix)"
  echo "$output"
  TIME_COUNTER="$((TIME_COUNTER+1))"
  sleep "$TIME_INTERVAL"
done
